//
//  EndPoint.swift
//  SupportI
//
//  Created by Mohamed Abdu on 3/20/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import Foundation

public enum EndPoint: String {
    case token
    case login = "auth/login"
    case forgetPassword = "auth/password/forget"
    case resetPassword = "auth/password/reset"
    case verifyCode = "auth/password/check-code"
    case places = "places"
    case features = "place_features"
    case menu = "place_menu"
    case reservations
    case tablesTypes = "gathering_types"
    case settings
    case pages = "static_pages"
    case page
    case notifications
    case coupons
    case countries
}
