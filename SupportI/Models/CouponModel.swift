// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let couponsModel = try? newJSONDecoder().decode(CouponsModel.self, from: jsonData)

import Foundation

// MARK: - CouponsModel
struct CouponsModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: [Datum]?
    let totalFolos: Int?
    
    enum CodingKeys: String, CodingKey {
        case status, error, message, data
        case totalFolos = "total_folos"
    }
    
    
    // MARK: - Datum
    struct Datum: Codable {
        let id: Int?
        let name, price: String?
        let start, end: String?
        let today: String?
        let expired: Int?
        let createdAt: String?
        let image: String?
        let usageNum: Int?
        
        enum CodingKeys: String, CodingKey {
            case id, name, price, start, end, today, expired
            case createdAt = "created_at"
            case image
            case usageNum = "usage_num"
        }
    }

}
