// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let reviewModel = try? newJSONDecoder().decode(ReviewModel.self, from: jsonData)

import Foundation

// MARK: - ReviewModel
struct ReviewModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: [Datum]?
    let data2: Data2?
    
    
    // MARK: - Datum
    struct Datum: Codable {
        let id, serviceRate, foodRate, placeRate: Double?
        let rateComment: String?
        let user, place: Place?
        let createdAt: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case serviceRate = "service_rate"
            case foodRate = "food_rate"
            case placeRate = "place_rate"
            case rateComment = "rate_comment"
            case user, place
            case createdAt = "created_at"
        }
    }
    
    // MARK: - Place
    struct Place: Codable {
        let id: Int?
        let name, image: String?
    }
    
    // MARK: - Data2
    struct Data2: Codable {
        let rateCount: Int?
        let allServiceRate, allFoodRate: Double?
        let allPlaceRate: Double?
        
        enum CodingKeys: String, CodingKey {
            case rateCount = "rate_count"
            case allServiceRate = "all_service_rate"
            case allFoodRate = "all_food_rate"
            case allPlaceRate = "all_place_rate"
        }
    }

    
}
