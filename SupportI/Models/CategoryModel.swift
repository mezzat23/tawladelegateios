// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let tablesModel = try? newJSONDecoder().decode(TablesModel.self, from: jsonData)

import Foundation

// MARK: - TablesModel
struct CategoryModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: DataClass?
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let currentPage: Int?
        let data: [Datum]?
        let firstPageURL: String?
        let from, lastPage: Int?
        let lastPageURL, path: String?
        let perPage, to, total: Int?
        
        enum CodingKeys: String, CodingKey {
            case currentPage = "current_page"
            case data
            case firstPageURL = "first_page_url"
            case from
            case lastPage = "last_page"
            case lastPageURL = "last_page_url"
            case path
            case perPage = "per_page"
            case to, total
        }
    }
    
    // MARK: - Datum
    struct Datum: Codable {
        let id: Int?
        let createdAt, updatedAt, name: String?
        let translations: [Translation]?
        
        enum CodingKeys: String, CodingKey {
            case id
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case name, translations
        }
    }
    
    // MARK: - Translation
    struct Translation: Codable {
        let id, menuCatID: Int?
        let name, locale: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case menuCatID = "menu_cat_id"
            case name, locale
        }
    }

}
