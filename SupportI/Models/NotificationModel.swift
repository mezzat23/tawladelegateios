// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let notificationModel = try? newJSONDecoder().decode(NotificationModel.self, from: jsonData)

import Foundation

// MARK: - NotificationModel
struct NotificationModel: Codable {
    let count: Int?
    let haveMessage: String?
    let allURL: String?
    let allMessage: String?
    let data: [Datum]?
    let previousMessage, nextMessage: String?
    let links: Links?
    let meta: Meta?
    
    enum CodingKeys: String, CodingKey {
        case count
        case haveMessage = "have_message"
        case allURL = "all_url"
        case allMessage = "all_message"
        case data
        case previousMessage = "previous_message"
        case nextMessage = "next_message"
        case links, meta
    }
    
    
    // MARK: - Datum
    struct Datum: Codable {
        let type, title, body: String?
        let url: String?
        let data: DataClass?
        let date: String?
    }
    
    // MARK: - DataClass
    struct DataClass: Codable {
        let type: String?
        let id, targetKey: Int?
        let targetType, title, body: String?
        let translateData: TranslateData?
        let dashboardRoute: String?
        let dashboardRouteData: [Int]?
        let route: String?
        let routeData: [Int]?
        
        enum CodingKeys: String, CodingKey {
            case type, id
            case targetKey = "target_key"
            case targetType = "target_type"
            case title, body
            case translateData = "translate_data"
            case dashboardRoute = "dashboard_route"
            case dashboardRouteData = "dashboard_route_data"
            case route
            case routeData = "route_data"
        }
    }
    
    // MARK: - TranslateData
    struct TranslateData: Codable {
        let reservation: Int?
        let date, customer, place: String?
    }
    
    // MARK: - Links
    struct Links: Codable {
        let first, last: String?
    }
    
    // MARK: - Meta
    struct Meta: Codable {
        let currentPage, from, lastPage: Int?
        let path: String?
        let perPage, to, total: Int?
        
        enum CodingKeys: String, CodingKey {
            case currentPage = "current_page"
            case from
            case lastPage = "last_page"
            case path
            case perPage = "per_page"
            case to, total
        }
    }
}

