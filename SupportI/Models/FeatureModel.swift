// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let tablesModel = try? newJSONDecoder().decode(TablesModel.self, from: jsonData)

import Foundation

// MARK: - TablesModel
struct FeatureModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: [PlaceModel.PlaceType]?
    
}
