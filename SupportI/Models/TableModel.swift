// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let tablesModel = try? newJSONDecoder().decode(TablesModel.self, from: jsonData)

import Foundation

// MARK: - TablesModel
struct TablesModel: Codable {
    let status: String?
    let error: Bool?
    let message: String?
    let data: [Datum]?
    
    // MARK: - Datum
    struct Datum: Codable {
        let id, tableTypeID, placeID, number: Int?
        let capacityFrom, capacityTo, hasBarrier: Int?
        let position: String?
        let price, createdAt, updatedAt: String?
        let tableType: TableType?
        
        enum CodingKeys: String, CodingKey {
            case id
            case tableTypeID = "table_type_id"
            case placeID = "place_id"
            case number
            case capacityFrom = "capacity_from"
            case capacityTo = "capacity_to"
            case hasBarrier = "has_barrier"
            case position, price
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case tableType = "table_type"
        }
    }
    
    // MARK: - TableType
    struct TableType: Codable {
        let id: Int?
        let createdAt: String?
        let updatedAt: String?
        let barrierable: Int?
        let pic: String?
        let name: String?
        let translations: [Translation]?
        
        enum CodingKeys: String, CodingKey {
            case id
            case createdAt = "created_at"
            case updatedAt = "updated_at"
            case barrierable, pic, name, translations
        }
    }
    
    // MARK: - Translation
    struct Translation: Codable {
        let id, tableTypeID: Int?
        let name: String?
        let locale: String?
        
        enum CodingKeys: String, CodingKey {
            case id
            case tableTypeID = "table_type_id"
            case name, locale
        }
    }

    
}

