//
//  NotificationWaiterModel.swift
//  SupportI
//
//  Created by M.abdu on 10/6/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import Foundation

//{table_num=15, smallIcon=small_icon, res_id=78, client_name=hhh, body=طلب نادل, type=Waiter, sound=1, title=Tawla App, vibrate=1, largeIcon=large_icon, message=طلب نادل, client_pic=https://shartec-sa.com/public/storage/445/avatar.jpeg}

struct NotificationWaiter: Codable {
    var table_num: String?
    var res_id: String?
    var client_name: String?
    var body: String?
    var type: String?
    var title: String?
    var message: String?
    var client_pic: String?
}
