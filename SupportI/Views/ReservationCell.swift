//
//  ReservationCell.swift
//  Tawla-client
//
//  Created by M.abdu on 7/9/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

protocol ReservationCellDelegate: class {
    func didAccept(path: Int)
    func didCancel(path: Int)
    func didRate()
    func didReOrder()
}
class ReservationCell: UITableViewCell, CellProtocol {
    @IBOutlet weak var resturantImage: UIImageView!
    @IBOutlet weak var resturantNameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var typeCountLbl: UILabel!
    @IBOutlet weak var tableNumLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    weak var delegate: ReservationCellDelegate?
    var isPrev: Bool = false
    func setup() {
        guard let model = model as? ReservationsModel.Datum else { return }
        resturantImage.setImage(url: model.customer?.avatar)
        resturantNameLbl.text = model.customer?.name
        phoneLbl.text = model.customer?.mobile
        dateLbl.text = model.date
        typeLbl.text = model.gatheringType?.name
        typeCountLbl.text = "\((model.adultCount ?? 0) + (model.childrenCount ?? 0))"
        tableNumLbl.text = model.table?.number?.string ?? "0"
        if isPrev {
//            editBtn.setImage(#imageLiteral(resourceName: "star"), for: .normal)
//            editBtn.setTitle("Rate Book".localized, for: .normal)
//
//            cancelBtn.setImage(#imageLiteral(resourceName: "bookings_nav-1"), for: .normal)
//            cancelBtn.setTitle("Reorder Book".localized, for: .normal)
            editBtn.isHidden = true
            cancelBtn.isHidden = true
        } else {
            if model.status == "approved" || model.status == "canceled" {
                editBtn.isHidden = true
                cancelBtn.isHidden = true
            } else {
                editBtn.isHidden = false
                cancelBtn.isHidden = false
            }
            //editBtn.isHidden = true
            //editBtn.setImage(#imageLiteral(resourceName: "booking_edit"), for: .normal)
            editBtn.setTitle("Accept Book".localized, for: .normal)
            
            cancelBtn.setImage(#imageLiteral(resourceName: "delete"), for: .normal)
            cancelBtn.setTitle("Cancel Book".localized, for: .normal)
        }
    }
    @IBAction func editBook(_ sender: Any) {
        if isPrev == true {
            delegate?.didRate()
        } else {
            delegate?.didAccept(path: indexPath())
        }
    }
    @IBAction func cancelBook(_ sender: Any) {
        if isPrev == true {
            delegate?.didReOrder()
        } else {
            delegate?.didCancel(path: indexPath())
        }
    }

    
}
