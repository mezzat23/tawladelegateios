//
//  TableCollectionCell.swift
//  SupportI
//
//  Created by M.abdu on 7/4/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

protocol TablesCollectionDelagate: class {
    func didAddNew()
}
class TableCollectionCell: UICollectionViewCell, CellProtocol {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var addNewView: UIView!
    
    weak var delegate: TablesCollectionDelagate?
    func setup() {
        guard let model = model as? TablesModel.Datum else { return setupNew() }
        addNewView.isHidden = true
        imageView.isHidden = false
        titleLbl.isHidden = false
        imageView.setImage(url: model.tableType?.pic)
        titleLbl.text = model.position
    }
    func setupNew() {
        addNewView.isHidden = true
        imageView.isHidden = true
        titleLbl.isHidden = true
        
//        addNewView.isHidden = false
//        imageView.isHidden = true
//        titleLbl.isHidden = true
//        addNewView.UIViewAction { [weak self] in
//            self?.delegate?.didAddNew()
//        }
    }
}
