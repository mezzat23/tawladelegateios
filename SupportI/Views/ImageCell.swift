//
//  ImageCell.swift
//  SupportI
//
//  Created by M.abdu on 7/4/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

protocol ImageCellDelegate: class {
    func didAddImage()
}
class ImageCell: UICollectionViewCell, CellProtocol {
    @IBOutlet weak var plusView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    weak var delegate: ImageCellDelegate?
    func setup() {
        guard let model = model as? PlaceModel.Album else { return setupPlus() }
        imageView.isHidden = false
        plusView.isHidden = true
        imageView.setImage(url: model.link)
    }
    func setupPlus() {
        imageView.isHidden = true
        //plusView.isHidden = false
        plusView.isHidden = true
    }
    @IBAction func addImage(_ sender: Any) {
        self.delegate?.didAddImage()
    }
}
