//
//  Comments.swift
//  SupportI
//
//  Created by Adam on 3/22/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
import Cosmos
class Comments: UITableViewCell, CellProtocol {
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var reviewLbl: UILabel!
    
    func setup() {
        guard let model = model as? ReviewModel.Datum else { return }
        dateLbl.text = model.createdAt
        userImage.setImage(url: model.user?.image)
        usernameLbl.text = model.user?.name
        rateView.rating = model.placeRate ?? 0
        reviewLbl.text = model.rateComment
    }
}
