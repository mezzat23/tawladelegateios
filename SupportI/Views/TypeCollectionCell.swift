//
//  TypeCollectionCell.swift
//  SupportI
//
//  Created by M.abdu on 7/15/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class TypeCollectionCell: UICollectionViewCell, CellProtocol {
    @IBOutlet weak var typeTitleBtn: UIButton!
    
    func setup() {
        guard let model = model as? TableTypeModel.Datum else { return }
        typeTitleBtn.setTitle(model.name, for: .normal)
    }

}
