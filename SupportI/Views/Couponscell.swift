//
//  Coupons.swift
//  SupportI
//
//  Created by Adam on 4/6/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class Couponscell: UITableViewCell, CellProtocol {
    @IBOutlet weak var couponTitleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var startDateLbl: UILabel!
    @IBOutlet weak var endDateLbl: UILabel!
    @IBOutlet weak var benefitsLbl: UILabel!
    
    func setup() {
        guard let model = model as? CouponsModel.Datum else { return }
        couponTitleLbl.text = model.name
        dateLbl.text = model.createdAt
        startDateLbl.text = model.start
        endDateLbl.text = model.end
        benefitsLbl.text = model.usageNum?.string
        if model.expired == 1 {
            statusLbl.text = "Expired coupon".localized
        } else {
            statusLbl.text = "Effective coupon".localized
        }
    }
    
}
