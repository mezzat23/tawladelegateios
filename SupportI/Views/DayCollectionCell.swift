//
//  DayCollectionCell.swift
//  SupportI
//
//  Created by M.abdu on 7/4/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class DayCollectionCell: UICollectionViewCell, CellProtocol {
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var containView: UIView!
    
    var days: [PlaceModel.WorkingDay] = []
    var daysName: [String] = []
    func setup() {
        if path == 0 {
            nameLbl.text = "All days".localized
            if days.count > 6 {
                nameLbl.textColor = .white
                containView.backgroundColor = .appColor
            } else {
                nameLbl.textColor = .appDarkGrey
                containView.backgroundColor = .textFieldBackGround
            }
        } else {
            let name = daysName[(path ?? 0) - 1]
            nameLbl.text = name
            nameLbl.textColor = .appDarkGrey
            containView.backgroundColor = .textFieldBackGround
            if days.count > 6 {
                return
            }
            for day in days {
                var checker = false
                if day.day == name {
                    checker = true
                    nameLbl.textColor = .white
                    containView.backgroundColor = .appColor
                    break
                }
            }
        }
    }
}
