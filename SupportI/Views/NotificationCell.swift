//
//  NotificationCell.swift
//  Tawla-client
//
//  Created by M.abdu on 5/29/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell, CellProtocol {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var notificationImage: UIImageView!
    
    func setup() {
        guard let model = model as? NotificationModel.Datum else { return }
        titleLbl.text = model.title
        dateLbl.text = model.date
        notificationImage.setImage(url: "image")
    }
}
