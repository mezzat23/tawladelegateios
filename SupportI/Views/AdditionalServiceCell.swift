//
//  AdditionalServiceCell.swift
//  SupportI
//
//  Created by M.abdu on 7/4/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class AdditionalServiceCell: UITableViewCell, CellProtocol {
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var radioBtn: RadioButton!
    
    var placeFeatures: [PlaceModel.PlaceType] = []
    func setup() {
        guard let model = model as? PlaceModel.PlaceType else { return }
        titleLbl.text = model.name
        descLbl.isHidden = true
        for feature in placeFeatures {
            radioBtn.deselect()
            if feature.id == model.id {
                radioBtn.select()
                break
            }
        }
    }
}
