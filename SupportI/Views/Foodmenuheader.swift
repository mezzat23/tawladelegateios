//
//  Foodmenuheader.swift
//  SupportI
//
//  Created by Adam on 3/19/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit


class Foodmenuheader: UITableViewCell, CellProtocol {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var itemsHeight: NSLayoutConstraint!
    @IBOutlet weak var itemsTbl: UITableView! {
        didSet {
            itemsTbl.delegate = self
            itemsTbl.dataSource = self
        }
    }
    var menu: FoodMenuModel.Datum?
    var fromOrder: Bool = false
    var heightPerOne: CGFloat = 108
    func setup() {
        guard let model = model as? FoodMenuModel.Datum else { return }
        menu = model
        titleLbl.text = menu?.name
        itemsTbl.reloadData()
        itemsHeight.constant = heightPerOne * (model.menuItems?.count ?? 0).cgFloat
    }
    
}
// MARK:  Table view
extension Foodmenuheader: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu?.menuItems?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.cell(type: MenuItemQuantity.self, indexPath)
        let model = menu?.menuItems?[indexPath.row]
        if model?.pic != nil {
            cell.itemImage.setImage(url: model?.pic)
        } else {
            cell.itemImage.setImage(url: model?.image)
        }
        cell.itemNameLbl.text = model?.name
        cell.itemDescLbl.text = model?.details
        cell.priceLbl.text = "\(model?.price ?? "") \("SAR".localized)"
        cell.menu = model
        cell.setup()
        if fromOrder {
            cell.shppingView.isHidden = false
        } else {
            cell.shppingView.isHidden = true
            
        }
        return cell
    }
}
