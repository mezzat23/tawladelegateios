//
//  Storyboards.swift
//  SupportI
//
//  Created by Mohamed Abdu on 3/20/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import Foundation

public enum Storyboards: String {
    case main = "Main"
    case auth = "Auth"
    case account = "Account"
    case tables = "Tables"
    case pop = "Popups"
    case setting = "Settings"
    case reservations = "Reservations"
    case pickerHelper = "PickerViewHelper"
}
