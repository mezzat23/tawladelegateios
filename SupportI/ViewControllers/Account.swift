////
////  Account.swift
////  SupportI
////
////  Created by Adam on 3/31/20.
////  Copyright © 2020 MohamedAbdu. All rights reserved.
////
//
//import UIKit
//import XLPagerTabStrip
//
//class Account: ButtonBarPagerTabStripViewController {
//    var navigationBarHeight:Int! = 77
//
//    override func viewDidLoad() {
//        settings.style.buttonBarBackgroundColor = UIColor.white
//        settings.style.buttonBarHeight = CGFloat(55)
//        settings.style.selectedBarBackgroundColor = UIColor.orange
//        settings.style.selectedBarHeight = 2
//        settings.style.buttonBarItemBackgroundColor = UIColor.white
//        settings.style.buttonBarItemFont = UIFont.systemFont(ofSize: 18)
//        settings.style.buttonBarItemsShouldFillAvailableWidth = true
//
//        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
//            guard changeCurrentIndex == true else { return }
//            oldCell?.label.textColor = UIColor.black
//            newCell?.label.textColor = UIColor.orange
//        }
//        func shadow(radius:CGFloat ,height:CGFloat,opacity:Float, color:UIColor){
//            self.buttonBarView.layer.shadowColor = color.cgColor
//            self.buttonBarView.layer.shadowOffset = CGSize(width: 0, height: height)
//            self.buttonBarView.layer.shadowOpacity = 0.5
//            self.buttonBarView.layer.shadowRadius = 1
//            self.buttonBarView.layer.masksToBounds = false
//        }
//        super.viewDidLoad()
//       self.containerView.isScrollEnabled = false
//             self.buttonBarView.frame.origin.y+=CGFloat(100)
//    }
//    override public func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
//      let viewcontrollers:[String] = ["SummaryAccount","TableAccount","Menu","Reviews"]
//                    
//                    let child = UIStoryboard(name: Constants.storyboard, bundle: nil).instantiateViewController(withIdentifier: viewcontrollers[0]) as! SummaryAccount
//                    let child1 = UIStoryboard(name: Constants.storyboard, bundle: nil).instantiateViewController(withIdentifier: viewcontrollers[1]) as! TableAccount
//                    let child2 = UIStoryboard(name: Constants.storyboard, bundle: nil).instantiateViewController(withIdentifier: viewcontrollers[2]) as! Menu
//                    let child3 = UIStoryboard(name: Constants.storyboard, bundle: nil).instantiateViewController(withIdentifier: viewcontrollers[3]) as! Reviews
//           return [child, child1,child2,child3]
//        
//    }
//    
//}
