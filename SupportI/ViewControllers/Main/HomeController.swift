//
//  Home.swift
//  SupportI
//
//  Created by M.abdu on 7/2/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import Foundation
import UIKit
import Cosmos
class HomeController: TabBarController {
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var memberSinceLbl: UILabel!
    @IBOutlet weak var userRate: CosmosView!
    @IBOutlet weak var addNewView: UIView!
    @IBOutlet weak var bookingLbl: UILabel!
    @IBOutlet weak var tableLbl: UILabel!
    @IBOutlet weak var reviewsLbl: UILabel!
    @IBOutlet weak var productLbl: UILabel!
    @IBOutlet weak var couponsView: UIView!
    @IBOutlet weak var draftsView: UIView!
    @IBOutlet weak var statistics: UIView!
    @IBOutlet weak var notificationsView: UIView!
    @IBOutlet weak var settingsView: UIView!
    @IBOutlet weak var languageView: UIView!
    @IBOutlet weak var supportView: UIView!
    
    var place: PlaceModel.DataClass?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        handlers()
        fetchPlace()
        reload()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        handleNotification()
    }
    func handleNotification() {
        if TabBarController.notification != nil {
            let entity = TabBarController.notification
            let scene = controller(CallWaiterController.self, storyboard: .pop)
            scene.notification = entity
            pushPop(vcr: scene)
            TabBarController.notification = nil
        }
    }
    override func setup() {
        super.setup()
        reload()
        
    }
    func reload() {
        userImage.setImage(url: place?.image)
        usernameLbl.text = place?.name
        let date = DateHelper().date(date: place?.createdAt, format: "MMM d, yyyy", oldFormat: "dd-MM-yyyy")
        memberSinceLbl.text = date
        userRate.rating = place?.rate?.double() ?? 0
        bookingLbl.text = place?.reservationsNum?.string
        reviewsLbl.text = place?.ratesNum?.string
        tableLbl.text = place?.placeTablesNum?.string
        productLbl.text = place?.menuNum?.string
    }
    func handlers() {
        draftsView.UIViewAction {
            let scene = self.controller(TablesController.self, storyboard: .account)
            scene.isDraft = true
            self.push(scene)
        }
        couponsView.UIViewAction {
            let scene = self.controller(CouponsController.self, storyboard: .account)
            self.push(scene)
        }
        statistics.UIViewAction {
            let scene = self.controller(StatisticsController.self, storyboard: .account)
            self.push(scene)
        }
        notificationsView.UIViewAction {
            let scene = self.controller(NotificationsController.self, storyboard: .account)
            self.push(scene)
        }
        settingsView.UIViewAction {
            let scene = self.controller(SettingController.self, storyboard: .setting)
            self.push(scene)
        }
        languageView.UIViewAction {
            changeLang {
                let controller = UIStoryboard(name: "Auth", bundle: nil).instantiateInitialViewController()
                guard let nav = controller else { return }
                Localizer.initLang()
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    Localizer.initLang()
                    let delegate = UIApplication.shared.delegate as? AppDelegate
                    delegate?.window?.rootViewController = nav
                }
            }
        }
        supportView.UIViewAction {
            
        }
    }
    @IBAction func qrCode(_ sender: Any) {
        let scene = controller(QrcodeController.self, storyboard: .pop)
        scene.delegate = self
        pushPop(vcr: scene)
    }
    @IBAction func logout(_ sender: Any) {
        makeAlert("Are you sure ?".localized) {
            UserRoot.save(response: Data())
            guard let scene = UIStoryboard.init(name: "Auth", bundle: nil).instantiateInitialViewController() else { return }
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.window?.rootViewController = scene
        }
    }
}
// MARK:  Network
extension HomeController {
    func fetchPlace() {
        let method = api(.places, [placeID])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(PlaceModel.self, from: response ?? Data())
            self.place = data?.data
            self.reload()
        }
    }
}
extension HomeController: QrCodeDelegate {
    func didFindTable(for table: String?) {
        let scene = controller(BillDetailController.self, storyboard: .reservations)
        scene.reservationID = table
        push(scene)
    }
}
