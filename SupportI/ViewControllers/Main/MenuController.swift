//
//  Menu.swift
//  SupportI
//
//  Created by Adam on 4/5/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class MenuController: AccountStackController {
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTxf: UITextField!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var menuTbl: UITableView! {
        didSet {
            menuTbl.delegate = self
            menuTbl.dataSource = self
        }
    }
    

    //var model: MenuFoodModel.DataClass?
    var menues: [FoodMenuModel.Datum] = []
    var categories: [CategoryModel.Datum] = []
    var paramters: [String: Any] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchCats()
        setup()
        handlers()
        // Do any additional setup after loading the view.
    }
    override func setup() {
        super.setup()
        self.menuTbl.reloadData()
    }
    func handlers() {
        categoryView.UIViewAction(selector: pickerCategories)
        searchTxf.delegate = self
    }
    func pickerCategories() {
        let scene = controller(PickerViewHelper.self, storyboard: .pickerHelper)
        scene.source = []
        scene.source.append(contentsOf: categories)
        scene.didSelectClosure = { row in
            if row == 0 {
                self.categoryLbl.text = "Category".localized
                self.paramters["cat"] = nil
                self.fetchMenu()
            } else {
                let category = self.categories[row]
                self.categoryLbl.text = category.name
                self.paramters["cat"] = category.id
                self.fetchMenu()
            }
            
        }
        scene.titleClosure = { row in
            return self.categories[row].name
        }
        pushPop(vcr: scene)
    }
    @IBAction func newItem(_ sender: Any) {
    }
}

// MARK:  Network
extension MenuController {
    func fetchMenu() {
        let method = api(.places, [placeID, "place_menu"])
        startLoading()
        ApiManager.instance.paramaters = paramters
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(FoodMenuModel.self, from: response ?? Data() )
            //self.model = data?.data
            self.menues.removeAll()
            self.menues.append(contentsOf: data?.data?.data ?? [])

            self.setup()
        }
    }
    func fetchCats() {
        categories.append(CategoryModel.Datum(id: nil, createdAt: nil, updatedAt: nil, name: "Reset".localized, translations: []))
        let method = api(.places, [placeID, "cats"])
        startLoading()
        ApiManager.instance.connection(method, type: .get) { (response) in
            let data = try? JSONDecoder().decode(CategoryModel.self, from: response ?? Data() )
            self.categories.append(contentsOf: data?.data?.data ?? [])
            self.fetchMenu()
        }
    }
}
//extension MenuController: UITableViewDelegate, UITableViewDataSource {
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return model?.data?.count ?? 0
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        var cell = tableView.cell(type: Foodmenufooter.self, indexPath)
//        cell.model = model?.data?[indexPath.row]
//        return cell
//    }
//}
// MARK:  Table View
extension MenuController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: Foodmenuheader.self, indexPath)
        cell.model = menues[indexPath.row]
        return cell
    }
}

extension MenuController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        paramters["name"] = textField.text
        fetchMenu()
    }
}
