//
//  Home.swift
//  SupportI
//
//  Created by M.abdu on 7/2/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import Foundation
import UIKit

class AccountStackController: TabBarController {
    @IBOutlet weak var summaryView: UIView!
    @IBOutlet weak var tablesView: UIView!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var reviewsView: UIView!
    @IBOutlet weak var summaryUnderline: UIView!
    @IBOutlet weak var tablesUnderline: UIView!
    @IBOutlet weak var menuUnderline: UIView!
    @IBOutlet weak var reviewsUnderline: UIView!
    @IBOutlet weak var summaryBtn: UIButton!
    @IBOutlet weak var tablesBtn: UIButton!
    @IBOutlet weak var menuFoodBtn: UIButton!
    @IBOutlet weak var reviewsBtn: UIButton!
    
    static var index: Int = 0
    static var indexDraft: Int = 0
    var isDraft: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        if isDraft {
            setupDraftIndex()
        } else {
            setupIndex()
        }
        // Do any additional setup after loading the view.
    }
    
    override func setup() {
    }
    func setupIndex() {
        switch AccountStackController.index {
            case 0:
                summaryUnderline.backgroundColor = .appColor
                tablesUnderline.backgroundColor = .clear
                menuUnderline.backgroundColor = .clear
                reviewsUnderline.backgroundColor = .clear
            
                summaryBtn.setTitleColor(.appColor, for: .normal)
                tablesBtn.setTitleColor(.black, for: .normal)
                menuFoodBtn.setTitleColor(.black, for: .normal)
                reviewsBtn.setTitleColor(.black, for: .normal)
            case 1:
                summaryUnderline.backgroundColor = .clear
                tablesUnderline.backgroundColor = .appColor
                menuUnderline.backgroundColor = .clear
                reviewsUnderline.backgroundColor = .clear
            
                summaryBtn.setTitleColor(.black, for: .normal)
                tablesBtn.setTitleColor(.appColor, for: .normal)
                menuFoodBtn.setTitleColor(.black, for: .normal)
                reviewsBtn.setTitleColor(.black, for: .normal)
            case 2:
                summaryUnderline.backgroundColor = .clear
                tablesUnderline.backgroundColor = .clear
                menuUnderline.backgroundColor = .appColor
                reviewsUnderline.backgroundColor = .clear
            
                summaryBtn.setTitleColor(.black, for: .normal)
                tablesBtn.setTitleColor(.black, for: .normal)
                menuFoodBtn.setTitleColor(.appColor, for: .normal)
                reviewsBtn.setTitleColor(.black, for: .normal)
            case 3:
                summaryUnderline.backgroundColor = .clear
                tablesUnderline.backgroundColor = .clear
                menuUnderline.backgroundColor = .clear
                reviewsUnderline.backgroundColor = .appColor
            
                summaryBtn.setTitleColor(.black, for: .normal)
                tablesBtn.setTitleColor(.black, for: .normal)
                menuFoodBtn.setTitleColor(.black, for: .normal)
                reviewsBtn.setTitleColor(.appColor, for: .normal)
            default:
             break
        }
    }
    func setupDraftIndex() {
        switch AccountStackController.indexDraft {
            case 0:
                tablesUnderline.backgroundColor = .appColor
                menuUnderline.backgroundColor = .clear
                
                tablesBtn.setTitleColor(.appColor, for: .normal)
                menuFoodBtn.setTitleColor(.black, for: .normal)
            case 1:
                tablesUnderline.backgroundColor = .clear
                menuUnderline.backgroundColor = .appColor
                
                tablesBtn.setTitleColor(.black, for: .normal)
                menuFoodBtn.setTitleColor(.appColor, for: .normal)
            default:
                break
        }
    }

    override func backBtn(_ sender: Any) {
        if isDraft {
            AccountStackController.indexDraft = 0
            for controller in self.navigationController?.viewControllers ?? [] {
                if controller is UITabBarController {
                    self.navigationController?.popToViewController(controller, animated: true)
                }
            }
        } else {
            super.backBtn(sender)
        }
    }
    @IBAction func summary(_ sender: Any) {
        AccountStackController.index = 0
        guard let scene = UIStoryboard.init(name: "Main", bundle: nil).instantiateInitialViewController() as? UITabBarController else { return }
        scene.selectedIndex = 2
        self.push(scene)
    }
    @IBAction func tables(_ sender: Any) {
        if isDraft {
            AccountStackController.indexDraft = 0
            let scene = self.controller(TablesController.self, storyboard: .account)
            scene.isDraft = true
            self.push(scene)
        } else {
            AccountStackController.index = 1
            let scene = self.controller(TablesController.self, storyboard: .main)
            self.push(scene)
        }
    
    }
    @IBAction func menus(_ sender: Any) {
        if isDraft {
            AccountStackController.indexDraft = 1
            let scene = self.controller(MenuController.self, storyboard: .account)
            scene.isDraft = true
            self.push(scene)
        } else {
            AccountStackController.index = 2
            let scene = self.controller(MenuController.self, storyboard: .main)
            self.push(scene)
        }

    }
    @IBAction func reviews(_ sender: Any) {
        AccountStackController.index = 3
        let scene = self.controller(ReviewsController.self, storyboard: .main)
        self.push(scene)
    }
    
}
