//
//  Home.swift
//  SupportI
//
//  Created by M.abdu on 7/2/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import Foundation
import UIKit

class TabBarController: BaseController {
    @IBOutlet weak var closeSwitch: UISwitch!
    
    var placeID: Int {
        return UserRoot.fetch()?.place?.first ?? 0
    }
    var openPlace: Bool = false
    static var notification: NotificationWaiter?
    override func viewDidLoad() {
        super.viewDidLoad()
        if closeSwitch != nil {
            //closeSwitch.isHidden = true
        }
        self.fetchPlaceTab()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    func setup() {
    }
    
    @IBAction func closeChange(_ sender: Any) {
        if self.openPlace {
            let method = "places/close/\(placeID)"
            startLoading()
            ApiManager.instance.paramaters["closed"] = 0
            
            ApiManager.instance.connection(method, type: .post) { (response) in
                self.stopLoading()
                self.openPlace = false
                self.fetchPlaceTab()
            }
        } else {
            closeSwitch.setOn(!closeSwitch.isOn, animated: false)
            let scene = controller(CloseController.self, storyboard: .pop)
            scene.closure = {
                self.fetchPlaceTab()
            }
            pushPop(vcr: scene)
        }
        
    }
}
// MARK:  Network
extension TabBarController {
    func fetchPlaceTab() {
        let method = api(.places, [placeID])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(PlaceModel.self, from: response ?? Data())
            if data?.data?.closed == 1 {
                if self.closeSwitch != nil {
                    self.closeSwitch.setOn(false, animated: false)
                }
                self.openPlace = true
            } else {
                if self.closeSwitch != nil {
                    self.closeSwitch.setOn(true, animated: false)
                }
                self.openPlace = false
            }
        }
    }
}
