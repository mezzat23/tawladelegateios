//
//  TableAccount.swift
//  SupportI
//
//  Created by Adam on 3/31/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class TablesController: AccountStackController {
    @IBOutlet weak var tablesCollection: UICollectionView! {
        didSet {
            tablesCollection.delegate = self
            tablesCollection.dataSource = self
        }
    }
    
    var tables: [TablesModel.Datum] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchTables()
        // Do any additional setup after loading the view.
    }
    func reload() {
        tablesCollection.reloadData()
    }
}
// MARK:  Network
extension TablesController {
    func fetchTables() {
        startLoading()
        let method = api(.places, [placeID, "tables"])
        ApiManager.instance.paramaters["position"] = "all"
        ApiManager.instance.paramaters["price"] = "all"
        ApiManager.instance.paramaters["type"] = "all"
        ApiManager.instance.paramaters["has_barrier"] = "all"
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(TablesModel.self, from: response ?? Data())
            self.tables.append(contentsOf: data?.data ?? [])
            self.reload()
        }
    }
}
extension TablesController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        if indexPath.row == 0 {
//            return CGSize(width: 70, height: 90)
//        } else {
//            return CGSize(width: 90, height: 90)
//        }
        
        return CGSize(width: 90, height: 90)

    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return tables.count+1
        return tables.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = collectionView.cell(type: TableCollectionCell.self, indexPath)
        cell.delegate = self
//        if indexPath.row == 0 {
//            cell.model = "NEW"
//        } else {
//            cell.model = tables[indexPath.row-1]
//        }
        cell.model = tables[indexPath.row]
        return cell
    }
    
}
extension TablesController: TablesCollectionDelagate {
    func didAddNew() {
        
    }
}
