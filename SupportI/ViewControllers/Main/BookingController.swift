//
//  Booking.swift
//  SupportI
//
//  Created by Adam on 3/18/20.
//
import UIKit

class BookingController: TabBarController {
    enum Tab {
        case today
        case tomorrow
        case current
        case prev
    }
    enum Price {
        case free
        case vip
    }
    @IBOutlet weak var finishedBtn: UIButton!
    @IBOutlet weak var currentBtn: UIButton!
    @IBOutlet weak var tomorrowBtn: UIButton!
    @IBOutlet weak var todayBtn: UIButton!
    @IBOutlet weak var todayUnderline: UIView!
    @IBOutlet weak var tomorrowUnderline: UIView!
    @IBOutlet weak var currentUnderline: UIView!
    @IBOutlet weak var prevUnderline: UIView!
    @IBOutlet weak var freeRadioBtn: RadioButton!
    @IBOutlet weak var vipRadioBtn: RadioButton!
    @IBOutlet weak var bookingTbl: UITableView!
    
    var tab: Tab = .today
    var price: Price?
    var paramters: [String: Any] = [:]
    var reservations: [ReservationsModel.Datum] = []
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        setup()
        handlers()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        paramters = [:]
        reservations.removeAll()
        fetchReservations()
    }
    override func setup() {
        super.setup()
        bookingTbl.delegate = self
        bookingTbl.dataSource = self
    }
    func handlers() {
        todayBtn.UIViewAction {
            self.tab = .today
            self.todayBtn.setTitleColor(.appColor, for: .normal)
            self.tomorrowBtn.setTitleColor(.black, for: .normal)
            self.currentBtn.setTitleColor(.black, for: .normal)
            self.finishedBtn.setTitleColor(.black, for: .normal)
            self.todayUnderline.backgroundColor = .appColor
            self.tomorrowUnderline.backgroundColor = .clear
            self.currentUnderline.backgroundColor = .clear
            self.prevUnderline.backgroundColor = .clear
            self.reservations.removeAll()
            self.fetchReservations()
        }
        tomorrowBtn.UIViewAction {
            self.tab = .tomorrow
            self.todayBtn.setTitleColor(.black, for: .normal)
            self.tomorrowBtn.setTitleColor(.appColor, for: .normal)
            self.currentBtn.setTitleColor(.black, for: .normal)
            self.finishedBtn.setTitleColor(.black, for: .normal)
            self.todayUnderline.backgroundColor = .clear
            self.tomorrowUnderline.backgroundColor = .appColor
            self.currentUnderline.backgroundColor = .clear
            self.prevUnderline.backgroundColor = .clear
            self.reservations.removeAll()
            self.fetchReservations()
        }
        currentBtn.UIViewAction {
            self.tab = .current
            self.todayBtn.setTitleColor(.black, for: .normal)
            self.tomorrowBtn.setTitleColor(.black, for: .normal)
            self.currentBtn.setTitleColor(.appColor, for: .normal)
            self.finishedBtn.setTitleColor(.black, for: .normal)
            self.todayUnderline.backgroundColor = .clear
            self.tomorrowUnderline.backgroundColor = .clear
            self.currentUnderline.backgroundColor = .appColor
            self.prevUnderline.backgroundColor = .clear
            self.reservations.removeAll()
            self.fetchReservations()
        }
        finishedBtn.UIViewAction {
            self.tab = .prev
            self.todayBtn.setTitleColor(.black, for: .normal)
            self.tomorrowBtn.setTitleColor(.black, for: .normal)
            self.currentBtn.setTitleColor(.black, for: .normal)
            self.finishedBtn.setTitleColor(.appColor, for: .normal)
            self.todayUnderline.backgroundColor = .clear
            self.tomorrowUnderline.backgroundColor = .clear
            self.currentUnderline.backgroundColor = .clear
            self.prevUnderline.backgroundColor = .appColor
            self.reservations.removeAll()
            self.fetchReservations()
        }
        freeRadioBtn.onSelect {
            self.price = .free
            self.reservations.removeAll()
            self.fetchReservations()
            self.vipRadioBtn.deselect()
        }
        freeRadioBtn.onDeselect {
            if self.price == .free {
                self.price = nil
                self.reservations.removeAll()
                self.fetchReservations()
            }
        }
        vipRadioBtn.onSelect {
            self.price = .vip
            self.reservations.removeAll()
            self.fetchReservations()
            self.freeRadioBtn.deselect()
        }
        vipRadioBtn.onDeselect {
            if self.price == .vip {
                self.price = nil
                self.reservations.removeAll()
                self.fetchReservations()
            }
        }
    }
    func reloadView() {
        if reservations.count == 0 {
            bookingTbl.isHidden = true
        } else {
            bookingTbl.isHidden = false
            bookingTbl.reloadData()
        }
    }
    func fetchReservations() {
        ApiManager.instance.paramaters = paramters
        if tab == .today {
            ApiManager.instance.paramaters["due_date"] = "today"
        } else if tab == .tomorrow {
            ApiManager.instance.paramaters["due_date"] = "tomorrow"
        } else if tab == .current {
            ApiManager.instance.paramaters["due_date"] = "later"
        } else {
            ApiManager.instance.paramaters["due_date"] = "finished"
        }
        //** price **/
        if price == .free {
            ApiManager.instance.paramaters["price"] = "free"
        } else if price == .vip {
            ApiManager.instance.paramaters["price"] = "vip"
        }
        startLoading()
        ApiManager.instance.connection(.reservations, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(ReservationsModel.self, from: response ?? Data())
            self.reservations.append(contentsOf: data?.data ?? [])
            self.reloadView()
        }
    }
    
    @IBAction func filter(_ sender: Any) {
        let scene = controller(FilterReservationController.self, storyboard: .pop)
        scene.delegate = self
        if paramters["date"] != nil {
            scene.oldDate = paramters["date"] as? String
        }
        if let position = paramters["position"] as? String {
            if position == "inside" {
                scene.oldPlace = .inside
            } else if position == "outside" {
                scene.oldPlace = .outside
            }
        }
        if paramters["gatheringType"] != nil {
            scene.oldType = paramters["gatheringType"] as? Int
        }
        pushPop(vcr: scene)
    }
   
}
extension BookingController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reservations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: ReservationCell.self, indexPath)
        if tab == .prev {
            cell.isPrev = true
        } else {
            cell.isPrev = false
        }
        cell.delegate = self
        cell.model = reservations[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = reservations[indexPath.row]
        if model.status == "approved" {
            let scene = controller(BillDetailController.self, storyboard: .reservations)
            scene.reservation = model
            push(scene)
        }

//        let scene = controller(ReservationDetailController.self, storyboard: .reservation)
//        scene.reservation = reservations[indexPath.row]
//        push(scene)
    }
    
}
extension BookingController: ReservationCellDelegate {
    func didAccept(path: Int) {
        let scene = controller(CancelOrderController.self, storyboard: .pop)
        scene.isAccept = true
        scene.reservationID = reservations[path].id
        scene.closureRefresh = {
            self.paramters = [:]
            self.reservations.removeAll()
            self.fetchReservations()
        }
        pushPop(vcr: scene)
    }
    func didCancel(path: Int) {
        let scene = controller(CancelOrderController.self, storyboard: .pop)
        scene.reservationID = reservations[path].id
        scene.closureRefresh = {
            self.paramters = [:]
            self.reservations.removeAll()
            self.fetchReservations()
        }
        pushPop(vcr: scene)
    }
    
    func didRate() {
        
    }
    
    func didReOrder() {
        
    }
    
    
}
extension BookingController: FilterReservationDelegate {
    func didFilter(place: FilterReservationController.TablePlace, typeID: Int?, date: String?) {
        if date != nil {
            paramters["date"] = date
        }
        if typeID != nil {
            paramters["gatheringType"] = typeID ?? 0
        }
        if place == .inside {
            paramters["position"] = "inside"
        } else if place == .outside {
            paramters["position"] = "outside"
        }
        self.reservations.removeAll()
        self.fetchReservations()
    }
}
