//
//  Home.swift
//  SupportI
//
//  Created by M.abdu on 7/2/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import CountryList
class AccountController: AccountStackController {
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var mobileTxf: UITextField!
    @IBOutlet weak var addMobileLbl: UILabel!
    @IBOutlet weak var emailTxf: UITextField!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var locationTxf: UITextField!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var workingDaysLbl: UILabel!
    @IBOutlet weak var daysCollection: UICollectionView!
    @IBOutlet weak var fromTimeView: UIView!
    @IBOutlet weak var toTimeView: UIView!
    @IBOutlet weak var fromTimeLbl: UILabel!
    @IBOutlet weak var toTimeLbl: UILabel!
    @IBOutlet weak var dishesCollection: UICollectionView!
    @IBOutlet weak var dishesHeight: NSLayoutConstraint!
    @IBOutlet weak var addDishLbl: UILabel!
    @IBOutlet weak var servicesTbl: UITableView!
    @IBOutlet weak var servicesHeight: NSLayoutConstraint!
    @IBOutlet weak var bioTxv: UITextView!
    @IBOutlet weak var imagesCollection: UICollectionView!
    @IBOutlet weak var imagesHeight: NSLayoutConstraint!
    
    var dialCode: String = "+966"
    var place: PlaceModel.DataClass?
    var mapHelper: GoogleMapHelper?
    var location: LocationHelper?
    var daysEn = ["Saturday", "Sunday", "Monday", "Tuesday","Wednesday", "Thursday", "Friday"]
    var daysAr = ["السبت","الاحد", "الاثنين","الثلاثاء","الاربعاء","الخميس","الجمعة"]
    var features: [PlaceModel.PlaceType] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        handleCountry()
        setup()
        fetchFeatureData()
        stopEditable()
        // Do any additional setup after loading the view.
    }
    func stopEditable() {
        mobileTxf.isUserInteractionEnabled = false
        addMobileLbl.isUserInteractionEnabled = false
        emailTxf.isUserInteractionEnabled = false
        locationTxf.isUserInteractionEnabled = false
        bioTxv.isUserInteractionEnabled = false
    }
    func handleCountry() {
        countryLbl.text = "🇸🇦 966"
        let countries = CountryList()
        countries.delegate = self
        countryLbl.UIViewAction {
            let navController = UINavigationController(rootViewController: countries)
            self.present(navController, animated: true, completion: nil)
        }
    }
    override func setup() {
        super.setup()
        mapHelper = .init()
        mapHelper?.delegate = self
        mapHelper?.mapView = mapView
        emailTxf.addPadding(15)
        locationTxf.addPadding(15)
        daysCollection.delegate = self
        daysCollection.dataSource = self
        dishesCollection.delegate = self
        dishesCollection.dataSource = self
        servicesTbl.delegate = self
        servicesTbl.dataSource = self
        imagesCollection.delegate = self
        imagesCollection.dataSource = self
    }
    func reload() {
        mobileTxf.text = UserRoot.fetch()?.data?.mobile
        emailTxf.text = UserRoot.fetch()?.data?.email
        locationLbl.text = place?.city
        locationTxf.text = place?.address
        mapHelper?.updateCamera(lat: place?.latitude?.double() ?? 0, lng: place?.longitude?.double() ?? 0)
        workingDaysLbl.text = "\(place?.workingDays?.first?.day ?? "") \("to".localized) \(place?.workingDays?.last?.day ?? "")\n \(place?.workingSchedule ?? "")"
        daysCollection.reloadData()
        fromTimeLbl.text = place?.workingDays?.first?.startAt
        toTimeLbl.text = place?.workingDays?.first?.endAt
        dishesCollection.reloadData()
        dishesHeight.constant = dishesCollection.collectionViewLayout.collectionViewContentSize.height
        servicesTbl.reloadData()
        servicesHeight.constant = servicesTbl.contentSize.height + 50
        bioTxv.text = place?.details
        imagesCollection.reloadData()
        imagesHeight.constant = imagesCollection.collectionViewLayout.collectionViewContentSize.height
    }

    @IBAction func save(_ sender: Any) {
    }
}
// MARK:  Network
extension AccountController {
    func fetchPlace() {
        let method = api(.places, [placeID])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(PlaceModel.self, from: response ?? Data())
            self.place = data?.data
            self.reload()
        }
    }
    func fetchFeatureData() {
        startLoading()
        ApiManager.instance.connection(.features, type: .get) { (response) in
            let data = try? JSONDecoder().decode(FeatureModel.self, from: response ?? Data())
            self.features.removeAll()
            self.features.append(contentsOf: data?.data ?? [])
            self.fetchPlace()
        }
    }
}
extension AccountController: GoogleMapHelperDelegate {
    func didChangeCameraLocation(lat: Double, lng: Double) {
        mapHelper?.setMarker(position: CLLocationCoordinate2D(latitude: lat, longitude: lng))
    }
}
extension AccountController: CountryListDelegate {
    func selectedCountry(country: Country) {
        self.countryLbl.text = "\(country.flag ?? "") \(country.phoneExtension)"
        dialCode = country.phoneExtension
    }
}
extension AccountController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         if collectionView == daysCollection {
            //let widthCell = place?.workingDays?[indexPath.row].day?.widthWithConstrainedWidth(width: collectionView.width)
            return CGSize(width: 80 , height: 30)
         } else if collectionView == dishesCollection {
            let widthCell = place?.placeServiceType?[indexPath.row].name?.widthWithConstrainedWidth(width: collectionView.width) ?? 0
            return CGSize(width: widthCell + 10, height: 30)
         } else if collectionView == imagesCollection {
            return CGSize(width: 110, height: 110)
         } else {
            return CGSize(width: 0, height: 0)
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == daysCollection {
            return daysEn.count + 1
        } else if collectionView == dishesCollection {
            return place?.placeServiceType?.count ?? 0
        } else if collectionView == imagesCollection {
            return (place?.album?.count ?? 0) + 1
        }  else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == daysCollection {
            let cell = collectionView.cell(type: DayCollectionCell.self, indexPath)
            cell.days = place?.workingDays ?? []
            if Localizer.current == "ar" {
                cell.daysName = daysAr
            } else {
                cell.daysName = daysEn
            }
            cell.setup()
            return cell
        } else if collectionView == dishesCollection {
            let cell = collectionView.cell(type: DayCollectionCell.self, indexPath)
            cell.nameLbl.text = place?.placeServiceType?[indexPath.row].name
            cell.nameLbl.textColor = .white
            cell.containView.backgroundColor = .appColor
            return cell
        } else {
            var cell = collectionView.cell(type: ImageCell.self, indexPath)
            if indexPath.row == place?.album?.count ?? 0 {
                cell.model = "PLUS"
            } else {
                cell.model = place?.album?[indexPath.row]
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == daysCollection {
            var days: [String] = []
            if Localizer.current == "ar" {
                days = daysAr
            } else {
                days = daysEn
            }
            for day in place?.workingDays ?? [] {
                var checker = false
                if day.day == days[indexPath.row - 1] {
                    checker = true
                    fromTimeLbl.text = day.startAt
                    toTimeLbl.text = day.endAt
                    break
                }
            }
        }
    }
}
extension AccountController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return features.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: AdditionalServiceCell.self, indexPath)
        cell.placeFeatures = place?.placeFeatures ?? []
        cell.model = features[indexPath.row]
        return cell
    }
    
}
extension AccountController: ImageCellDelegate {
    func didAddImage() {
        
    }
}
