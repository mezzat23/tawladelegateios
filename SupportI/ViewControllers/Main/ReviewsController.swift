//
//  Reviews.swift
//  SupportI
//
//  Created by Adam on 4/5/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
import Cosmos

class ReviewsController: AccountStackController {
    @IBOutlet weak var totalRateLbl: UILabel!
    @IBOutlet weak var totalRateView: CosmosView!
    @IBOutlet weak var ratesLbl: UILabel!
  
    @IBOutlet weak var eatRate: CosmosView!
    @IBOutlet weak var placeRate: CosmosView!
    @IBOutlet weak var reservationRate: CosmosView!
    @IBOutlet weak var ratesTbl: UITableView!
    @IBOutlet weak var totalRateConstraint: NSLayoutConstraint!
    @IBOutlet weak var rate5Lbl: UILabel!
    
    var reviewModel: ReviewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }
    override func setup() {
        super.setup()
        self.reload()
        self.fetchReviews()
        if Localizer.current == "ar" {
            totalRateConstraint = totalRateConstraint.setMultiplier(multiplier: 1.050)
        }
        ratesTbl.delegate = self
        ratesTbl.dataSource = self
    }
    func reload() {
        totalRateLbl.text = reviewModel?.data2?.allPlaceRate?.int.string
        totalRateView.rating = reviewModel?.data2?.allPlaceRate?.int.double ?? 0
        ratesLbl.text = "\("Rates:".localized) \(reviewModel?.data2?.rateCount ?? 0)"
    
        placeRate.rating = reviewModel?.data2?.allPlaceRate ?? 0
        reservationRate.rating = reviewModel?.data2?.allServiceRate ?? 0
        eatRate.rating = reviewModel?.data2?.allFoodRate ?? 0
        
        ratesTbl.reloadData()
    }
}
// MARK:  Network
extension ReviewsController {
    func fetchReviews() {
        let method = api(.places, [placeID, "rates"])
        startLoading()
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(ReviewModel.self, from: response ?? Data())
            self.reviewModel = data
            self.reload()
        }
    }
}
extension ReviewsController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviewModel?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: Comments.self, indexPath)
        cell.model = reviewModel?.data?[indexPath.row]
        return cell
    }
    
    
}
