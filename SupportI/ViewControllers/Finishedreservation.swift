//
//  Finishedreservation.swift
//  SupportI
//
//  Created by Adam on 4/1/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
import XLPagerTabStrip
class Finishedreservation: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}
extension Finishedreservation : IndicatorInfoProvider{
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo("Finished")
    }
    
    
}
