//
//  Settingvc.swift
//  SupportI
//
//  Created by Adam on 4/14/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class SettingController: BaseController {
    @IBOutlet weak var notificationSwitch: UISwitch!
    @IBOutlet weak var reservationPolicyLbl: UILabel!
    @IBOutlet weak var returnPolicyLbl: UILabel!
    @IBOutlet weak var usagePolicyLbl: UILabel!
    
    var settingModel: SettingModel?
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        fetchSetting()
        setup()
        // Do any additional setup after loading the view.
    }
    

    func setup() {
        reservationPolicyLbl.UIViewAction {
            let scene = self.controller(TermsController.self, storyboard: .setting)
            scene.type = .policyReservation
            self.push(scene)
        }
        returnPolicyLbl.UIViewAction {
            let scene = self.controller(TermsController.self, storyboard: .setting)
            scene.type = .policyReturn
            self.push(scene)
        }
        usagePolicyLbl.UIViewAction {
            let scene = self.controller(TermsController.self, storyboard: .setting)
            scene.type = .policyUsage
            self.push(scene)
        }
    }
    func fetchSetting() {
        startLoading()
        ApiManager.instance.connection(.settings, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(SettingModel.self, from: response ?? Data())
            self.settingModel = data
        }
    }
    @IBAction func aboutTawla(_ sender: Any) {
        let scene = controller(AboutUsController.self, storyboard: .setting)
        push(scene)
    }
    @IBAction func contactUs(_ sender: Any) {
        let scene = controller(TermsController.self, storyboard: .setting)
        scene.type = .contactUS
        push(scene)
    }
    @IBAction func joinUs(_ sender: Any) {
        let scene = controller(TermsController.self, storyboard: .setting)
        scene.type = .join
        push(scene)
    }
    @IBAction func questions(_ sender: Any) {
        let scene = controller(TermsController.self, storyboard: .setting)
        scene.type = .question
        push(scene)
    }
    @IBAction func snapchat(_ sender: Any) {
        openUrl(text: settingModel?.data?.snab)
    }
    @IBAction func instagram(_ sender: Any) {
        openUrl(text: settingModel?.data?.instagram)
    }
    @IBAction func twitter(_ sender: Any) {
        openUrl(text: settingModel?.data?.twitter)
    }
    @IBAction func facebook(_ sender: Any) {
        openUrl(text: settingModel?.data?.facebook)
    }
}
