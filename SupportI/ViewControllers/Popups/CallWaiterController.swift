//
//  Askwaiter.swift
//  SupportI
//
//  Created by Adam on 4/6/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class CallWaiterController: BaseController {
    @IBOutlet weak var tableLbl: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var reserveIDLbl: UILabel!
    
    var notification: NotificationWaiter?
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }
    
    func setup() {
        tableLbl.text = "\("Table".localized) \(notification?.table_num ?? "")"
        userImage.setImage(url: notification?.client_pic)
        usernameLbl.text = notification?.client_name
        reserveIDLbl.text = notification?.res_id
    }
    @IBAction func confirmed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func skip(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func backBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
