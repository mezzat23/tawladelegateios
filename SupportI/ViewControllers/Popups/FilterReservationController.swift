//
//  Filterdata.swift
//  SupportI
//
//  Created by Adam on 4/8/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
protocol FilterReservationDelegate: class {
    func didFilter(place: FilterReservationController.TablePlace, typeID: Int?, date: String?)
}
class FilterReservationController: BaseController {
    enum TablePlace {
        case inside
        case outside
    }
    @IBOutlet weak var typesCollection: UICollectionView!
    @IBOutlet weak var insideBtn: UIButton!
    @IBOutlet weak var outsideBtn: UIButton!
    @IBOutlet weak var checkDateImage: UIImageView!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var types: [TableTypeModel.Datum] = []
    var selectedType: Int?
    var selectedDate: Bool = false
    var place: TablePlace = .inside
    var oldType: Int?
    var oldDate: String?
    var oldPlace: TablePlace?
    weak var delegate: FilterReservationDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        handlers()
        fetchTypes()
        // Do any additional setup after loading the view.
    }

    func setup() {
        typesCollection.delegate = self
        typesCollection.dataSource = self
        datePicker.minimumDate = Date()
        self.insideTable(insideBtn)
        if oldPlace == .outside {
            self.outsideTable(outsideBtn)
        }
        if oldDate != nil {
            self.selectedDate = true
            if let date = DateHelper().originalDate(original: oldDate ?? "", oldFormat: "yyyy-MM-dd") {
                datePicker.date = date
            }
        } else {
            self.selectedDate = false
            self.checkDateImage.tintColor = .lightGray
        }
    }
    func handlers() {
        checkDateImage.UIViewAction {
            if self.selectedDate {
                self.selectedDate = false
                self.checkDateImage.tintColor = .lightGray
            } else {
                self.selectedDate = true
                self.checkDateImage.tintColor = UIColor(red: 166/255, green: 206/255, blue: 57/255, alpha: 1)
            }
        }
     
    }
    func setupOldType() {
        var counter = 0
        for each in types {
            if each.id == oldType {
                selectedType = counter
                break
            }
            counter += 1
        }
    }
    override func backBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func outsideTable(_ sender: Any) {
        self.place = .outside
        self.outsideBtn.borderColor = .appColor
        self.outsideBtn.borderWidth = 1
        self.outsideBtn.backgroundColor = UIColor(red: 254/255, green: 240/255, blue: 232/255, alpha: 1)
        
        self.insideBtn.borderColor = UIColor(red: 234/255, green: 234/255, blue: 235/255, alpha: 1)
        self.insideBtn.borderWidth = 1
        self.insideBtn.backgroundColor = .white
    }
    @IBAction func insideTable(_ sender: Any) {
        self.place = .inside
        self.insideBtn.borderColor = .appColor
        self.insideBtn.borderWidth = 1
        self.insideBtn.backgroundColor = UIColor(red: 254/255, green: 240/255, blue: 232/255, alpha: 1)
        
        self.outsideBtn.borderColor = UIColor(red: 234/255, green: 234/255, blue: 235/255, alpha: 1)
        self.outsideBtn.borderWidth = 1
        self.outsideBtn.backgroundColor = .white
    }
    @IBAction func filter(_ sender: Any) {
        var date: String?
        if selectedDate {
            let datePickerDate = DateHelper().currentFullFormat(date: datePicker.date)
            date = DateHelper().date(date: datePickerDate, format: "yyyy-MM-dd")
        }
        
        self.dismiss(animated: true) {
            self.delegate?.didFilter(place: self.place, typeID: self.types[safe: self.selectedType ?? 0]?.id, date: date)
        }
    }
}
// MARK:  Netowrk
extension FilterReservationController {
    func fetchTypes() {
        startLoading()
        ApiManager.instance.connection(.tablesTypes, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(TableTypeModel.self, from: response ?? Data())
            self.types.append(contentsOf: data?.data ?? [])
            self.setupOldType()
            self.typesCollection.reloadData()
        }
    }
}
// MARK:  Collection
extension FilterReservationController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: collectionView.width/2 - 30, height: 40)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return types.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = collectionView.cell(type: TypeCollectionCell.self, indexPath)
        if selectedType == indexPath.row {
            cell.typeTitleBtn.borderColor = .appColor
            cell.typeTitleBtn.borderWidth = 1
            cell.typeTitleBtn.backgroundColor = UIColor(red: 254/255, green: 240/255, blue: 232/255, alpha: 1)
        } else {
            cell.typeTitleBtn.borderColor = UIColor(red: 234/255, green: 234/255, blue: 235/255, alpha: 1)
            cell.typeTitleBtn.borderWidth = 1
            cell.typeTitleBtn.backgroundColor = .white
        }
        cell.typeTitleBtn.UIViewAction {
            self.selectedType = indexPath.row
            collectionView.reloadData()
        }
        cell.model = types[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        selectedType = indexPath.row
//        collectionView.reloadData()
    }
}
