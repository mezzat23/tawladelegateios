//
//  Createcoupon.swift
//  SupportI
//
//  Created by Adam on 4/8/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
import CalendarDateRangePickerViewController
class CloseController: BaseController {
    enum CloseType {
        case date
        case nonDate
    }
    @IBOutlet weak var closeNowBtn: RadioButton!
    @IBOutlet weak var closeNowLbl: UILabel!
    @IBOutlet weak var periodBtn: RadioButton!
    @IBOutlet weak var periodLbl: UILabel!
    @IBOutlet weak var datesLbl: UILabel!
    
    var placeID: Int {
        return UserRoot.fetch()?.place?.first ?? 0
    }
    var closure: HandlerView?
    var startDate: Date?
    var endDate: Date?
    var type: CloseType?
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    func setup() {
        periodBtn.onSelect {
            self.closeNowLbl.textColor = .black
            self.periodLbl.textColor = .appColor
            self.type = .date
            let dateRangePickerViewController = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
            dateRangePickerViewController.delegate = self
            dateRangePickerViewController.minimumDate = Date()
            dateRangePickerViewController.maximumDate = Calendar.current.date(byAdding: .year, value: 2, to: Date())
            dateRangePickerViewController.selectedStartDate = Date()
            dateRangePickerViewController.selectedEndDate = Calendar.current.date(byAdding: .day, value: 10, to: Date())
            //dateRangePickerViewController.selectedColor = UIColor.red
            //dateRangePickerViewController.titleText = "Select Date Range"
            let navigationController = UINavigationController(rootViewController: dateRangePickerViewController)
            self.present(navigationController, animated: true, completion: nil)
            //self.pushPop(vcr: dateRangePickerViewController)
            self.closeNowBtn.deselect()
        }
        closeNowBtn.onSelect {
            self.closeNowLbl.textColor = .appColor
            self.periodLbl.textColor = .black
            self.type = .nonDate
            self.periodBtn.deselect()
        }
        closeNowBtn.onDeselect {
            if self.type == .nonDate {
                self.type = nil
            }
        }
        periodBtn.onDeselect {
            if self.type == .date {
                self.type = nil
                self.datesLbl.text = ""
            }
        }
    }
    @IBAction func confirm(_ sender: Any) {
        
        let method = "places/close/\(placeID)"
        startLoading()
        ApiManager.instance.paramaters["closed"] = 1
        if type == .date {
            let startDate = DateHelper().date(date: self.startDate, format: "yyyy-MM-dd")
            let endDate = DateHelper().date(date: self.endDate, format: "yyyy-MM-dd")
            ApiManager.instance.paramaters["closestartdate"] = startDate ?? ""
            ApiManager.instance.paramaters["closeenddate"] = endDate ?? ""
        }
    
        ApiManager.instance.connection(method, type: .post) { (response) in
            self.stopLoading()
            self.dismiss(animated: true, completion: {
                self.closure?()
            })
        }
    }
}
extension CloseController: CalendarDateRangePickerViewControllerDelegate {
    func didTapCancel() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func didTapDoneWithDateRange(startDate: Date!, endDate: Date!) {
        self.dismiss(animated: true, completion: nil)
        self.startDate = startDate
        self.endDate = endDate
        let start = DateHelper().date(date: startDate, format: "MMM d, yyyy")
        let end = DateHelper().date(date: endDate, format: "MMM d, yyyy")
        datesLbl.text = "\(start ?? "") - \(end ?? "")"
    }
}
