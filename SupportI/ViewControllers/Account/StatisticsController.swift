//
//  Notifications.swift
//  SupportI
//
//  Created by Adam on 3/26/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
import WebKit
class StatisticsController: BaseController {
    @IBOutlet weak var webView: WKWebView!
    var placeID: Int {
        return UserRoot.fetch()?.place?.first ?? 0
    }
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        setup()
    }
    func setup() {
        let url = URL(string: "https://shartec-sa.com/api/places/\(placeID)/statistics?lang=\(Localizer.current)")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }
    
}
