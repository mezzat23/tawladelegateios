//
//  Coupons.swift
//  SupportI
//
//  Created by Adam on 4/6/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class CouponsController: BaseController {
    @IBOutlet weak var balanceLbl: UILabel!
    @IBOutlet weak var couponTxf: UITextField!
    @IBOutlet weak var createBtn: UIButton!
    @IBOutlet weak var filterLbl: UILabel!
    @IBOutlet weak var couponsTbl: UITableView! {
        didSet {
            couponsTbl.delegate = self
            couponsTbl.dataSource = self
        }
    }
    var paramters: [String: Any] = [:]
    var coupons: [CouponsModel.Datum] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchCoupons()
        setup()
        // Do any additional setup after loading the view.
    }
    
    func setup() {
        filterLbl.UIViewAction {
            self.filter()
        }
    }
    
    func filter() {
        let scene = controller(PickerViewHelper.self, storyboard: .pickerHelper)
        scene.source = ["Expired coupon".localized, "Effective coupon".localized]
        scene.didSelectClosure = { row in
            if row == 0 {
                self.paramters["expired"] = 1
            } else {
                self.paramters["expired"] = 0
            }
            self.coupons.removeAll()
            self.fetchCoupons()
        }
        scene.titleClosure = { row in
            if row == 0 {
                return "Expired coupon".localized
            } else {
                return "Effective coupon".localized
            }
        }
        pushPop(vcr: scene)
    }
    @IBAction func filter(_ sender: Any) {
        self.filter()
    }
    @IBAction func create(_ sender: Any) {
    }
}
// MARK:  Network
extension CouponsController {
    func fetchCoupons() {
        startLoading()
        ApiManager.instance.paramaters = self.paramters
        let method = api(.coupons, [UserRoot.fetch()?.place?.first ?? 0])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(CouponsModel.self, from: response ?? Data())
            self.coupons.append(contentsOf: data?.data ?? [])
            self.balanceLbl.text = data?.totalFolos?.string
            self.couponsTbl.reloadData()
        }
    }
}
extension CouponsController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return coupons.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.cell(type: Couponscell.self, indexPath)
        cell.model = coupons[indexPath.row]
        return cell
    }
}


