//
//  Forgetpassword.swift
//  SupportI
//
//  Created by Adam on 3/31/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
import CountryList

class Forgetpassword: BaseController {
    @IBOutlet weak var flagImage: UIImageView!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var mobileTxf: UITextField!
    @IBOutlet weak var contactUsLbl: UILabel!
    
    var dialCode: String = "+966"

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        handleCountry()
        // Do any additional setup after loading the view.
    }
    func setup() {
        mobileTxf.addPadding(20)
        contactUsLbl.UIViewAction {
            let scene = self.controller(TermsController.self, storyboard: .setting)
            scene.type = .contactUS
            self.push(scene)
        }
    }

    @IBAction func confirm(_ sender: Any) {
        if mobileTxf.text?.first == "0" {
            mobileTxf.text?.removeFirst()
        }
        if !validate(txfs: [mobileTxf]) {
            return
        }
        forget()
    }
    
}
extension Forgetpassword: CountryListDelegate {
    func selectedCountry(country: Country) {
        self.countryLbl.text = "\(country.flag ?? "") \(country.phoneExtension)"
        dialCode = country.phoneExtension
    }
}
// MARK:  Network
extension Forgetpassword {
    func forget() {
        startLoading()
        ApiManager.instance.paramaters["mobile"] = dialCode + (mobileTxf.text ?? "")
        ApiManager.instance.connection(.forgetPassword, type: .post) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(UserRoot.self, from: response ?? Data())
            self.goToVerify(code: data?.code?.string)
        }
    }
    func goToVerify(code: String?) {
        let scene = controller(Verficationcode.self, storyboard: .auth)
        scene.code = code
        scene.mobile = dialCode + (mobileTxf.text ?? "")
        scene.closureRefresh = { token in
            let scene = self.controller(Confirmpass.self, storyboard: .auth)
            scene.token = token
            self.push(scene)
        }
        pushPop(vcr: scene)
    }
}

// MARK:  Country
extension Forgetpassword {
    func handleCountry() {
        if Localizer.current == "ar" {
            countryLbl.text = "966 🇸🇦"
        } else {
            countryLbl.text = "🇸🇦 966"
        }
        fetchCountriesSetFirst()
        let countries = CountryList()
        countries.delegate = self
        countryLbl.UIViewAction {
            self.fetchCountries()
            
            //            let navController = UINavigationController(rootViewController: countries)
            //            self.present(navController, animated: true, completion: nil)
        }
        flagImage.UIViewAction {
            self.fetchCountries()
        }
    }
    func openPickerCountry(countries: [CountryModel.Datum]) {
        let scene = controller(PickerViewHelper.self, storyboard: .pickerHelper)
        scene.source = []
        scene.source.append(contentsOf: countries)
        scene.didSelectClosure = { row in
            self.countryLbl.text = countries[row].code
            self.dialCode = countries[row].code ?? ""
            self.flagImage.setImage(url: countries[row].flag)
        }
        scene.titleClosure = { row in
            return countries[row].name
        }
        pushPop(vcr: scene)
    }
    func fetchCountries() {
        startLoading()
        ApiManager.instance.connection(.countries, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(CountryModel.self, from: response ?? Data())
            self.openPickerCountry(countries: data?.data ?? [])
        }
    }
    func fetchCountriesSetFirst() {
        ApiManager.instance.connection(.countries, type: .get) { (response) in
            let data = try? JSONDecoder().decode(CountryModel.self, from: response ?? Data())
            let countries = data?.data ?? []
            self.countryLbl.text = countries.first?.code
            self.dialCode = countries.first?.code ?? ""
            self.flagImage.setImage(url: countries.first?.flag)
        }
    }
}
