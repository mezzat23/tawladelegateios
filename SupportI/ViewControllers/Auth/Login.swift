//
//  Login.swift
//  SupportI
//
//  Created by Adam on 3/31/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit
import CountryList

class Login: BaseController {
    @IBOutlet weak var flagImage: UIImageView!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var mobileTxf: UITextField!
    @IBOutlet weak var passwordTxf: UITextField!
    @IBOutlet weak var joinUsLbl: UILabel!
    
    var dialCode: String = "+966"
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserRoot.token() != nil {
            goToHome()
            return
        }
        setup()
        handleCountry()
        // Do any additional setup after loading the view.
    }
    func setup() {
        mobileTxf.addPadding(20)
        passwordTxf.addPadding(20)
        joinUsLbl.UIViewAction {
            let scene = self.controller(TermsController.self, storyboard: .setting)
            scene.type = .join
            self.push(scene)
        }
    }
   

    @IBAction func show(_ sender: Any) {
        passwordTxf.isSecureTextEntry = !passwordTxf.isSecureTextEntry
    }
    @IBAction func login(_ sender: Any) {
        if mobileTxf.text?.first == "0" {
            mobileTxf.text?.removeFirst()
        }
        if !validate(txfs: [mobileTxf, passwordTxf]) {
            return
        }
        makeLogin()
    }
    @IBAction func forgetPassword(_ sender: Any) {
        let scene = controller(Forgetpassword.self, storyboard: .auth)
        push(scene)
    }
}
extension Login: CountryListDelegate {
    func selectedCountry(country: Country) {
        self.countryLbl.text = "\(country.flag ?? "") \(country.phoneExtension)"
        dialCode = country.phoneExtension
    }
}
// MARK:  Network
extension Login {
    func makeLogin() {
        startLoading()
        ApiManager.instance.paramaters["mobile"] = dialCode + (mobileTxf.text ?? "")
        //ApiManager.instance.paramaters["mobile"] = "966544299430"
        ApiManager.instance.paramaters["password"] = passwordTxf.text
        ApiManager.instance.paramaters["type"] = "place_owner"
        ApiManager.instance.paramaters["platform"] = "2"
        if let devicetoken = UserDefaults.standard.string(forKey: "deviceToken") {
            ApiManager.instance.paramaters["registrationid"] = devicetoken
        } else {
            ApiManager.instance.paramaters["registrationid"] = "nil"
        }
        ApiManager.instance.connection(.login, type: .post) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(UserRoot.self, from: response ?? Data())
            data?.save()
            self.goToHome()
        }
    }
    func goToHome() {
        guard let scene = UIStoryboard(name: Storyboards.main.rawValue, bundle: nil).instantiateInitialViewController() else { return }
        push(scene)
    }
}

// MARK:  Country
extension Login {
    func handleCountry() {
        if Localizer.current == "ar" {
            countryLbl.text = "966 🇸🇦"
        } else {
            countryLbl.text = "🇸🇦 966"
        }
        self.fetchCountriesSetFirst()

        let countries = CountryList()
        countries.delegate = self
        countryLbl.UIViewAction {
            self.fetchCountries()
            
            //            let navController = UINavigationController(rootViewController: countries)
            //            self.present(navController, animated: true, completion: nil)
        }
        flagImage.UIViewAction {
            self.fetchCountries()
        }
    }
    func openPickerCountry(countries: [CountryModel.Datum]) {
        let scene = controller(PickerViewHelper.self, storyboard: .pickerHelper)
        scene.source = []
        scene.source.append(contentsOf: countries)
        scene.didSelectClosure = { row in
            self.countryLbl.text = countries[row].code
            self.dialCode = countries[row].code ?? ""
            self.flagImage.setImage(url: countries[row].flag)
        }
        scene.titleClosure = { row in
            return countries[row].name
        }
        pushPop(vcr: scene)
    }
    func fetchCountries() {
        startLoading()
        ApiManager.instance.connection(.countries, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(CountryModel.self, from: response ?? Data())
            self.openPickerCountry(countries: data?.data ?? [])
        }
    }
    func fetchCountriesSetFirst() {
        ApiManager.instance.connection(.countries, type: .get) { (response) in
            let data = try? JSONDecoder().decode(CountryModel.self, from: response ?? Data())
            let countries = data?.data ?? []
            self.countryLbl.text = countries.first?.code
            self.dialCode = countries.first?.code ?? ""
            self.flagImage.setImage(url: countries.first?.flag)
        }
    }
}
