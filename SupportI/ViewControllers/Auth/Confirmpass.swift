//
//  Confirmpass.swift
//  SupportI
//
//  Created by Adam on 3/31/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class Confirmpass: BaseController {
    @IBOutlet weak var passwordTxf: UITextField!
    @IBOutlet weak var confirmPassTxf: UITextField!
    @IBOutlet weak var termsLbl: UILabel!
    
    var token: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }
    func setup() {
        passwordTxf.addPadding(20)
        confirmPassTxf.addPadding(20)
        termsLbl.UIViewAction {
            let scene = self.controller(TermsController.self, storyboard: .setting)
            scene.type = .terms
            self.push(scene)
        }
    }
    @IBAction func newPassShow(_ sender: Any) {
        passwordTxf.isSecureTextEntry = !passwordTxf.isSecureTextEntry
    }
    @IBAction func confirmPassShow(_ sender: Any) {
        confirmPassTxf.isSecureTextEntry = !confirmPassTxf.isSecureTextEntry
    }
    @IBAction func confirm(_ sender: Any) {
        if !validate(txfs: [passwordTxf, confirmPassTxf]) {
            return
        }
        reset()
    }
    
}
// MARK:  Network
extension Confirmpass {
    func reset() {
        startLoading()
        ApiManager.instance.paramaters["token"] = token
        ApiManager.instance.paramaters["type"] = "PlaceOwner"
        ApiManager.instance.paramaters["password"] = passwordTxf.text
        ApiManager.instance.paramaters["password_confirmation"] = confirmPassTxf.text
        ApiManager.instance.connection(.resetPassword, type: .post) { (response) in
            self.stopLoading()
            //let data = try? JSONDecoder().decode(UserRoot.self, from: response ?? Data())
            self.goToLogin()
        }
    }
    func goToLogin() {
        makeAlert("Password reset successfully you want to login now ?".localized) {
            let scene = self.controller(Login.self, storyboard: .auth)
            self.push(scene)
        }
    }
}
