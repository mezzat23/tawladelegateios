//
//  Verficationcode.swift
//  SupportI
//
//  Created by Adam on 3/31/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class Verficationcode: BaseController {
    @IBOutlet weak var counterLbl: UILabel!
    @IBOutlet weak var resendStack: UIStackView!
    @IBOutlet weak var code1Txf: UITextField!
    @IBOutlet weak var code2Txf: UITextField!
    @IBOutlet weak var code3Txf: UITextField!
    @IBOutlet weak var code4Txf: UITextField!
    
    var code: String?
    var mobile: String?
    var timerHelper: TimeHelper?
    var closureRefresh: ((String?) -> Void)?
    var codeTxfs: String {
        return "\(code1Txf.text ?? "")\(code2Txf.text ?? "")\(code3Txf.text ?? "")\(code4Txf.text ?? "")"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }
    func setup() {
        code1Txf.delegate = self
        code2Txf.delegate = self
        code3Txf.delegate = self
        code4Txf.delegate = self
        code1Txf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        code2Txf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        code3Txf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        code4Txf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        
        timerHelper = .init(seconds: 1, numberOfCycle: 120, closure: setupTimerLbl)
    }
    func refresh() {
        timerHelper = .init(seconds: 1, numberOfCycle: 120, closure: setupTimerLbl)
        resendStack.isHidden = true
    }
    func setupTimerLbl(counter: Int) {
        counterLbl.text = "00 : \(timerHelper?.secondsTimer(cycle: counter) ?? "00")"

        
        if counter == 0 {
            counterLbl.text = ""
            timerHelper?.stopTimer()
            timerHelper = nil
            resendStack.isHidden = false
        }
    }
    override func backBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func resend(_ sender: Any) {
        resendCode()
    }
    @IBAction func confirm(_ sender: Any) {
        if !validate(txfs: [code1Txf, code2Txf, code3Txf, code4Txf]) {
            return
        }
        if codeTxfs != code {
            self.makeAlert("The code is incorrect".localized, closure: {})
            return
        }
        verifyCode()
    }
}
// MARK:  Network
extension Verficationcode {
    func resendCode() {
        startLoading()
        ApiManager.instance.paramaters["mobile"] = mobile
        ApiManager.instance.connection(.forgetPassword, type: .post) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(UserRoot.self, from: response ?? Data())
            self.code = data?.code?.string
            self.refresh()
        }
    }
    func verifyCode() {
        startLoading()
        ApiManager.instance.paramaters["mobile"] = mobile
        ApiManager.instance.paramaters["code"] = code
        ApiManager.instance.connection(.verifyCode, type: .post) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(BaseModel.self, from: response ?? Data())
            self.goToReset(token: data?.token)
        }
    }
    func goToReset(token: String?) {
        self.dismiss(animated: true, completion: {
            self.closureRefresh?(token)
        })
    }
}

extension Verficationcode: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
            case code1Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code1Txf.text = ""
                    }
            }
            case code2Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code2Txf.text = ""
                    }
            }
            case code3Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code3Txf.text = ""
                    }
            }
            case code4Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code4Txf.text = ""
                    }
            }
            default:
                break
        }
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        if Localizer.current == "ar" {
            return setupArabic(textField)
        }
        switch textField {
            case code1Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code2Txf.becomeFirstResponder()
                    } else {
                        view.endEditing(true)
                    }
            }
            case code2Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code3Txf.becomeFirstResponder()
                    } else {
                        code1Txf.becomeFirstResponder()
                    }
            }
            case code3Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code4Txf.becomeFirstResponder()
                    } else {
                        code2Txf.becomeFirstResponder()
                    }
            }
            case code4Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        view.endEditing(true)
                    } else {
                        code3Txf.becomeFirstResponder()
                    }
            }
            default:
                break
        }
    }
    func setupArabic(_ textField: UITextField) {
        switch textField {
            case code4Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code3Txf.becomeFirstResponder()
                    } else {
                        view.endEditing(true)
                    }
            }
            case code3Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code2Txf.becomeFirstResponder()
                    } else {
                        code4Txf.becomeFirstResponder()
                    }
            }
            case code2Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        code1Txf.becomeFirstResponder()
                    } else {
                        code3Txf.becomeFirstResponder()
                    }
            }
            case code1Txf:
                if let text = textField.text {
                    if text.count > 0 {
                        view.endEditing(true)
                    } else {
                        code2Txf.becomeFirstResponder()
                    }
            }
            default:
                break
        }
    }
    
}
