//
//  Ordersucces.swift
//  SupportI
//
//  Created by Adam on 3/24/20.
//  Copyright © 2020 MohamedAbdu. All rights reserved.
//

import UIKit

class BillDetailController: BaseController {
    @IBOutlet weak var reserveIDLbl: UILabel!
    @IBOutlet weak var reserveDateLbl: UILabel!
    @IBOutlet weak var tableImage: UIImageView!
    @IBOutlet weak var tableCodeLbl: UILabel!
    @IBOutlet weak var tablePrice: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var productsTbl: UITableView!
    @IBOutlet weak var productsHeight: NSLayoutConstraint!
    @IBOutlet weak var initialValueLbl: UILabel!
    @IBOutlet weak var couponLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var qrCodeImage: UIImageView!
    @IBOutlet weak var completeOrderBtn: UIButton!
    
    var paramters: [String: Any] = [:]
    var placeID: Int {
        return UserRoot.fetch()?.place?.first ?? 0
    }
    var reservation: ReservationsModel.Datum?
    var reservationID: String?
    var inovice: InvoiceModel.DataClass?
    var total: String?
    //var coupon: CouponModel.DataClass?
    override func viewDidLoad() {
        super.hiddenNav = true
        super.viewDidLoad()
        setup()
        if reservationID != nil {
            completeOrderBtn.isHidden = true
        }
        fetchInovice()
        // Do any additional setup after loading the view.
    }
    
    func setup() {
        reserveDateLbl.text = inovice?.resDate
        tableImage.setImage(url: inovice?.table?.table?.tableType?.image)
        tableCodeLbl.text = "\("Table:".localized) \(inovice?.table?.table?.number?.string ?? "")"
        tablePrice.text = "\(inovice?.resTablePrice ?? "") \("SAR".localized)"
        typeLbl.text = "\("Gathering type:".localized) \(inovice?.table?.table?.tableType?.name ?? "")"
    }

    func setupInvoice() {
        
        tableImage.setImage(url: inovice?.table?.table?.tableType?.image)
        tableCodeLbl.text = "\("Table:".localized) \(inovice?.table?.table?.number?.string ?? "")"
        tablePrice.text = "\(inovice?.resTablePrice ?? "") \("SAR".localized)"
        typeLbl.text = "\("Gathering type:".localized) \(inovice?.table?.table?.tableType?.name ?? "")"
        
        self.total = inovice?.resSubTotalPrice?.string
        reserveIDLbl.text = inovice?.resID?.string
        reserveDateLbl.text = "\(inovice?.resDate ?? "") \(inovice?.resStartTime ?? "")"
        initialValueLbl.text = "\(inovice?.resSubTotalPrice?.string ?? "") \("SAR".localized)"
        couponLbl.text = "-\(inovice?.resCouponPrice ?? 0) \("SAR".localized)"
        taxLbl.text = "+\(inovice?.resVatPrice ?? 0) \("SAR".localized)"
        totalLbl.text = "\(inovice?.resTotalPrice?.string ?? "") \("SAR".localized)"
        
        if inovice?.products?.count ?? 0 > 0 {
            productsTbl.delegate = self
            productsTbl.dataSource = self
            productsTbl.isHidden = false
            productsHeight.constant = 0
            productsTbl.reloadData()
            productsTbl.layoutIfNeeded()
            productsHeight.constant = (108) * (inovice?.products?.count ?? 0).cgFloat
        }
    }
    func fetchInovice() {
        startLoading()
        let reserveID = reservation?.id ?? reservationID?.int ?? 0
        let method = api(.reservations, ["reservationsInvoice", reserveID, placeID])
        ApiManager.instance.connection(method, type: .get) { (response) in
            self.stopLoading()
            let data = try? JSONDecoder().decode(InvoiceModel.self, from: response ?? Data())
            self.inovice = data?.data
            self.setupInvoice()
        }
    }

    @IBAction func complete(_ sender: Any) {
//        if isQRCode {
//            return
//        }
//        startLoading()
//        setupParamters()
//        paramters["notes"] = ""
//        paramters["paid"] = "1"
//        paramters["coupon"] = coupon?.price ?? "0"
//        paramters["payment"] = "online"
//        let method = api(.reservations, [placeID ?? 0, "add"])
//        ApiManager.instance.paramaters = paramters
//        ApiManager.instance.connection(method, type: .post) { (response) in
//            BookModel.reset()
//            let data = try? JSONDecoder().decode(OrderCompleteModel.self, from: response ?? Data())
//            let scene = self.controller(OrderCompletedController.self, storyboard: .reservation)
//            scene.order = data?.data
//            self.push(scene)
//        }
    }
    
}
extension BillDetailController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return inovice?.products?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.cell(type: MenuItemQuantity.self, indexPath)
        let model = inovice?.products?[indexPath.row]
        cell.itemImage.setImage(url: model?.image)
        cell.itemNameLbl.text = model?.name
        cell.itemDescLbl.text = model?.details
        
        cell.priceLbl.text = "\(((model?.price?.double() ?? 0) * (model?.quantity ?? 0).double).decimal(1)) \("SAR".localized)"
        cell.qty = model?.quantity ?? 0
        cell.setup()
        cell.shppingView.isHidden = true
        cell.disable = true
        return cell
    }
}
